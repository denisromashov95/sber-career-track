package com.interview.demo.service;


import com.interview.demo.dto.LoginDTO;
import com.interview.demo.model.User;

public interface UserService {
  User create(LoginDTO user);
}
