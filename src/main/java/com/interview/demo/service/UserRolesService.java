package com.interview.demo.service;



import com.interview.demo.model.UserRoles;

import java.util.List;

public interface UserRolesService {
  List<UserRoles> getAllUserRoles(Long userId);

}
