package com.interview.demo.service.impl;

import com.interview.demo.dto.LoginDTO;
import com.interview.demo.model.Role;
import com.interview.demo.model.User;
import com.interview.demo.model.UserRoles;
import com.interview.demo.repository.RoleRepository;
import com.interview.demo.repository.UserRepository;
import com.interview.demo.repository.UserRoleRepository;
import com.interview.demo.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private final UserRoleRepository userRoleRepository;
  private final UserRepository userRepository;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;


  public UserServiceImpl(UserRoleRepository userRoleRepository, UserRepository userRepository,
      BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userRoleRepository = userRoleRepository;
    this.userRepository = userRepository;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }


  @Override
  public User create(LoginDTO user) {
    User newUser = new User();
    newUser.setLogin(user.getLogin());
    newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    Role role = new Role();
    role.setId(1L);
    newUser.setRole(role);
    newUser = userRepository.save(newUser);
    UserRoles userRoles = new UserRoles();
    userRoles.setUser(newUser);
    userRoles.setRole(role);
    userRoleRepository.save(userRoles);
    return newUser;
  }
}

/*
INSERT INTO public.users (id, login, password)
VALUES (1, 'test', '$2a$10$8owJMVTTglp4cbEKvxFq7.qQMl0xdcFiC/IUMBiINsOXgrwAuBg5K');

INSERT INTO public.roles (id, title)
VALUES (1, 'user');
INSERT INTO public.roles (id, title)
VALUES (2, 'admin');

INSERT INTO public.user_roles (id, role_id, user_id)
VALUES (1, 1, 1);
 */
