package com.interview.demo.repository;

import com.interview.demo.model.UserRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRoles, Long> {

  List<UserRoles> findAllByUserId(Long id);

}
