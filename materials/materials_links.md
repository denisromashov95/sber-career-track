# SPRING / Hibernate:

1) https://habr.com/ru/post/529210/ - вопросы собеседований часть1
2) https://habr.com/ru/post/529214/ - вопросы собеседований часть2
3) https://habr.com/ru/post/428548/ - SPRING AOP
https://habr.com/ru/articles/347752/ - Spring AOP. Маленький вопросик с собеседования
https://habr.com/ru/articles/597797/ - Spring AOP: как работает проксирование
4) https://habr.com/ru/post/682362/ - про транзакции в SPRING
5) https://www.sibinfo.ru/about/articles/java-spring-sobesedovanie.html - вопросы спринг
6) https://habr.com/ru/post/350682/ - вопросы спринг продолжение
7) https://habr.com/ru/post/544472/ - интересные вопросы по спрингу
8) https://habr.com/ru/amp/post/708374/ - Hibernate устраняем пробелы
9) https://javastudy.ru/interview/jee-spring-questions-answers/ - вопросы spring с пояснениями
10) https://javastudy.ru/interview/jee-hibernate-questions-answers/ - вопросы хибернейт
11) https://habr.com/ru/post/440734/ - про open session in view
12) https://habr.com/ru/post/567368/ - rollback при исключении в транзакции


# Java и прочее:

1) Общие вопросы для любого уровня: https://habr.com/ru/post/505700/
2) https://github.com/enhorse/java-interview - СУПЕР ПОЛЕЗНЫЙ СБОРНИК МАТЕРИАЛОВ И ВОПРОСОВ ДЛЯ ПОДГОТОВКИ
3) https://github.com/enhorse/java-interview/blob/master/concurrency.md#Расскажите-о-модели-памяти-java - МНОГОПОТОЧНОСТЬ
4) https://habr.com/ru/post/555920/ - ACID свойства
5) https://javarush.com/groups/posts/3243-razbor-voprosov-i-otvetov-na-sobesedovanii-chastjh-1 - 12 статей по разбору вопросов с собеседований
6) https://habr.com/ru/post/108016/ - многопоточность
7) https://habr.com/ru/post/237043/ - коллекции (Справочник и подсказки)
8) https://habr.com/ru/post/129037/ - структуры данных в картинках
9) https://tproger.ru/translations/stacks-and-queues-for-beginners/ - про стэк и очереди
10) https://tproger.ru/articles/ponimaem-krasno-chjornoe-derevo-chast-1-vvedenie/ - деревья
11) https://roadmap.sh/java/ - список что вообще можно изучать
12) Визуализация 16 сортировок
Написано полностью на Java. Предупреждаем, можете залипнуть.
Исходники на GitHub: https://github.com/w0rthy/ArrayVisualizer
13) https://habr.com/ru/company/vk/blog/321306/  очень интересная статья про hashCode()




# Архитектура:

1) https://habr.com/ru/company/oleg-bunin/blog/319526/ - масшабирование
2) https://www.youtube.com/playlist?list=PL8X2nqRlWfaZcyrJrsrWmQ17vtagWKv3f - микросервисы на Spring Cloud
3) https://habr.com/ru/company/productivity_inside/blog/505430/ - SOLID
4) Книга "Чистая Архитектура"
5) https://habr.com/ru/post/483374/ - как разработать хороший REST API
6) https://habr.com/ru/post/326986/ - про GraphQL
7) https://habr.com/ru/post/491556/ - микросервисы в джава
8) https://habr.com/ru/post/682720/ - микросервисы на спрингбут, докер, мониторинг на Grafana
9) https://habr.com/ru/company/mtt/blog/711052/ - Обзор чат-бота ChatGPT
10) https://habr.com/ru/post/438808/ - МодельМаппер
11) https://github.com/voitau/system-design-primer/blob/master/README-ru.md - СИСТЕМНЫЙ ДИЗАЙН! ОЧЕНЬ ВАЖНАЯ ССЫЛКА!
